#  Основные отличия от оригинальной библиотеки

В оригинальной версии канонические заголовки находятся в отдельном слайсе и всегда находятся вверху списка, затем идут все остальные заголовки. 

Оригинал всегда будет иметь следующий порядок:

    Host 
    User-Agent
    Прочие заголовки...
	
В нашем случае такое поведение недопустимо, и должна быть возможность установить заголовки в нужном порядке.

Текущий порядок заголовков в Chrome:

    Host
    Connection: keep-alive
    Upgrade-Insecure-Requests: 1
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3
    Sec-Fetch-Site: none
    Sec-Fetch-Mode: navigate
	Referrer
    Accept-Encoding: gzip, deflate, br
    Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6,es;q=0.5,de;q=0.4
	Cookie 
